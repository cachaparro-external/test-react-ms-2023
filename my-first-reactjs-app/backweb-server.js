import axios from 'axios';
import express from 'express';
import bodyParser from 'body-parser';
import dotenv from 'dotenv';
import path from 'path';

//Obteniendo una instancia de Express
const app = express();
app.use(bodyParser.json());

//Configuración de DotEnv
//dotenv.config()
let envFilename = ".env" + (process.env.NODE_ENV ? '.' + process.env.NODE_ENV : '');

console.log("File: " + envFilename);

dotenv.config(
{
    path: path.resolve(process.cwd(), envFilename)
});

//Puerto de escucha
const port = process.env.BACKWEB_PORT || 3001;

const MAIN_URL = process.env.URL_GO_REST;
const TOKEN_BEARER = process.env.TOKEN_GO_REST;

app.post("/users", (req, res) => {
    let user = req.body;

    const config = {
        headers: {
            Authorization: "Bearer " + TOKEN_BEARER
        }
    };
    
    axios.post(MAIN_URL, user, config)
        .then(response => {
            res.status(response.status);

            if(response.status === 201){
                res.json(response.data);
            }else{
                res.send();
            }
        })
        .catch(error => {
            if(error.response){
                res.status(error.response.status);
                
                if(error.response.data){
                    res.json(error.response.data);
                }else{
                    res.send();
                }
            }else{
                res.status(500).send(error.message);
            }
        });
});

app.get("/users/:id", (req, res) => {
    let user = req.body;

    const config = {
        headers: {
            Authorization: "Bearer " + TOKEN_BEARER
        }
    };
    
    axios.post(MAIN_URL, user, config)
        .then(response => {
            res.status(response.status);

            if(response.status === 201){
                res.json(response.data);
            }else{
                res.send();
            }
        })
        .catch(error => {
            if(error.response){
                res.status(error.response.status);
                
                if(error.response.data){
                    res.json(error.response.data);
                }else{
                    res.send();
                }
            }else{
                res.status(500).send(error.message);
            }
        });
});

app.post("/provider", (req, res) => {
    let user = req.body;

    const config = {
        headers: {
            Authorization: "Bearer " + TOKEN_BEARER
        }
    };
    
    axios.post(MAIN_URL, user, config)
        .then(response => {
            res.status(response.status);

            if(response.status === 201){
                res.json(response.data);
            }else{
                res.send();
            }
        })
        .catch(error => {
            if(error.response){
                res.status(error.response.status);
                
                if(error.response.data){
                    res.json(error.response.data);
                }else{
                    res.send();
                }
            }else{
                res.status(500).send(error.message);
            }
        });
});

//Subir el servidor
app.listen(port, () => {
    //console.log("Envvar: " + JSON.stringify(process.env));
    //console.log("HOME: " + process.env.HOME);
    //console.log("Ambiente de ejecución: " + process.env.NODE_ENV);
    console.log("HOME: " + process.env.MY_APP_HOME);
    console.log("Server running on port " + port);
});