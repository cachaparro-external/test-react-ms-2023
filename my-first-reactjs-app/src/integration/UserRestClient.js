import axios from "axios";

class UserRestClient{
    //MAIN_URL = "/users";

    constructor(){
        
    }

    async invokeCreate(user){
        return axios.post("/users", user)
            .then(response => {
                if(response.status === 201){
                    console.log("OK con ID: " + response.data.id);

                    //return Promise.resolve(response.data.id);
                    return response.data.id;
                }else{
                    console.log("Retorno un código HTTP no válido " + response.status);

                    throw new Error('Código de retorno HTTP ' + response.status + ' no valido'); 
                }
            }).catch(error => {
                if(error.response){
                    if(error.response.status === 422){
                        throw new Error('El correo electronico ya existe'); 
                    }else{
                        console.log("Retorno un código HTTP no válido " + error.response.status);

                        throw new Error('Código de retorno HTTP ' + error.response.status + ' no valido'); 
                    }
                }else{
                    console.error("Se presento un error: " + error);
                    throw error;
                }
            });
    }

    getUserById(id){
        
    }
};

export default UserRestClient;