import { memo } from "react";

function PanelMessage(props){
    return (
        <>
            <div>
                {props.showErrorMsg && 
                    <div className="alert alert-danger" role="alert">
                        {props.errorMsg || ''}
                    </div>
                }
                {props.showWarnMsg && 
                    <div className="alert alert-warning" role="alert">
                        {props.warnMsg || ''}
                    </div>
                }
                {props.showSuccessMsg && 
                    <div className="alert alert-success" role="alert">
                        {props.successMsg || ''}
                    </div>
                }
            </div>      
        </>
    );
};

export default memo (PanelMessage);