import './Main.css';

import Header from './Header.js';
import Footer from './Footer.js';
import Central from './Central.js';

function Main(props){
    return (
        <>
         <div className="main">
            <div id="header" className='header'>
                <Header />
            </div>
            <div className='main-central'>
                <Central />
            </div>
            <div id="footer" className='footer'>
                <Footer />
            </div>
        </div>
        </>
    );
};

export default Main;