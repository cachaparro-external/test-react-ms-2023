import { Link, Outlet } from "react-router-dom";
import './Menu.css';

function Menu(props){
    return (
        <>    
       
    <nav>
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/form1">Estudiantes</Link>
          </li>
          <li>
            <Link to="/form2">Profesor</Link>
          </li>
        </ul>
      </nav>

            <Outlet />
        
        </>
    );
};

export default Menu;