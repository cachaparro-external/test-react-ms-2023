import { BrowserRouter, Routes, Route } from "react-router-dom";
import Form1 from "../form1/Form1.js";
import Home from './Home.js';
import Menu from "./Menu.js";
import Form2Function from './../form1/Form2Function.js';
import NoPage from './NoPage.js';
import './Main.css';

function Central(props){
    return (
        <>
        <BrowserRouter>
            <div id="menu" className='central-menu'>
                <Menu />      
            </div>
            <div id="central" className='central-main'>
                <Routes>
                    <Route path="/">
                        <Route index element={<Home />} />
                        <Route path="/form1" element={<Form1 />} /> 
                        <Route path="/form2" element={<Form2Function />} />
                        <Route path="*" element={<NoPage />} />
                    </Route>
                </Routes>    
            </div>
        </BrowserRouter>
        </>
    );
};

export default Central;