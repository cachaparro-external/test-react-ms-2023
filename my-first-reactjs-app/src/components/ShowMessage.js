function ShowMessage(props){
    return (
        <>
        <div>
            {props.pintarError &&
                <div className="alert alert-danger" role="alert">
                {props.mensajeError || ''}
                </div>
            }
            
            {props.pintarWarn &&
                <div className="alert alert-warning" role="alert">
                 {props.mensajeWarn || ''}
                </div>
            }

            {props.pintarSuccess &&
                <div className="alert alert-success" role="alert">
                 {props.mensajeSuccess || ''}
                </div>
            }
        </div>
        </>
    );
};

export default ShowMessage;