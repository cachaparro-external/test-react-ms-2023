import React from "react";
import Message from '../message/Message';
import UserRestClient from "../../integration/UserRestClient";
import PanelMessage from "../message/PanelMessage";

class Form2 extends React.Component{
    userRestClient;
    
    constructor(props){
        super(props);

        this.state = {
            fullName: "",
            email: "",
            gender: "F",
            showErrorMsg: false,
            showWarnMsg: false,
            showSuccessMsg: false,
            errorMsg: "",
            warnMsg: "",
            successMsg: ""
        };

        this.userRestClient = new UserRestClient();
    };

    clearMessage = () => {
        this.setState({
            showErrorMsg: false,
            showWarnMsg: false,
            showSuccessMsg: false,
            successMsg: '',
            warnMsg:  '',
            errorMsg: ''
        });
    }

    resetForm = () => {
        this.setState({
            fullName: "",
            email: "",
            gender: "F",
        });
    }

    handleChange = (event) => {
        let name = event.target.name;
        let value = event.target.value;

        this.setState({
            [name]: value
        });
    }

    executeSubmit = (event) => {
        event.preventDefault();
        this.clearMessage();

        const user = {
            name: this.state.fullName,
            email: this.state.email,
            gender: (this.state.gender === "F" ? "female" : "male"),
            status: "active"
        };

        let executionProm = this.userRestClient.invokeCreate(user);

        executionProm
            .then(idUsuario => {
                console.log("Se creo el usuario con el ID: " + idUsuario);

                this.setState({
                    showSuccessMsg: true,
                    successMsg: "Se creo el usuario con el ID: " + idUsuario
                });

                this.resetForm();
            })
            .catch(error => {
                console.error("Ocurrio un error " + error);

                this.setState({
                    showErrorMsg: true,
                    errorMsg: error.message
                });
            });
    };
    
    render(){
        return (
            <>
            <PanelMessage showErrorMsg={this.state.showErrorMsg} showWarnMsg={this.state.showWarnMsg} showSuccessMsg={this.state.showSuccessMsg}
                errorMsg={this.state.errorMsg} warnMsg={this.state.warnMsg} successMsg={this.state.successMsg}/>
            <Message fullName="Pedro Perez" />
            <form onSubmit={this.executeSubmit}>
                <div className="form-group">
                    <label htmlFor="fullName">Full name</label>
                    <input type="text" className="form-control" id="fullName" name="fullName" aria-describedby="fullName" placeholder="Enter full name" 
                        value={this.state.fullName} onChange={this.handleChange}/>
                </div>
                <div className="form-group">
                    <label htmlFor="email">Email address</label>
                    <input type="email" className="form-control" id="email" name="email" aria-describedby="email" placeholder="Enter email" 
                        value={this.state.email} onChange={this.handleChange}/>
                </div>
                <div className="form-group">
                    <label className="col-lg-6">Genero</label>
                    <div className="form-check form-check-inline">
                        <input className="form-check-input" type="radio" name="gender" id="genderF"
                            value="F" onChange={this.handleChange} required checked={this.state.gender === 'F'} />
                        <label className="form-check-label" htmlFor="genderF">Femenino</label>
                    </div>
                    <div className="form-check form-check-inline">
                        <input className="form-check-input" type="radio" name="gender" id="genderM" value="M"
                            onChange={this.handleChange} checked={this.state.gender === 'M'} />
                        <label className="form-check-label" htmlFor="genderM">Masculino</label>
                    </div>
                </div>
                <button type="submit" className="btn btn-primary">Submit</button>
            </form>
            </>
        );
    }
};

export default Form2;