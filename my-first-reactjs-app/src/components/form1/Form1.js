import React from "react";
import Message from '../message/Message.js';
import CommonValidateForm from '../CommonValidateForm.js'

class Form1 extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            username: "",
            password: "",
            comments: "",
            tipoId: "",
            gender: "O",
            estudiante: true
        };
    };

    executeSubmit = (event) => {
        event.preventDefault();
        console.log("Username: " + this.state.username + " - Password: " + this.state.password);
        console.log("Comments: " + this.state.comments + " - tipoId: " + this.state.tipoId);
        console.log("Gender: " + this.state.gender + " - estudiante: " + this.state.estudiante);
    };

    setUsername(username) {
        this.setState({
            username: username
        });
    }

    setPassword(password) {
        this.setState({
            password: password
        });
    }

    setComments(comments) {
        this.setState({
            comments: comments
        });
    }

    setTipoId(tipoId) {
        this.setState({
            tipoId: tipoId
        });
    }

    setGender(gender) {
        this.setState({
            gender: gender
        });
    }

    setEstudiante(estudiante) {
        this.setState({
            estudiante: !this.state.estudiante
        });
    }

    render() {
        return (
            <>
                <Message fullName="Pedro Perez" />
                <form onSubmit={this.executeSubmit}>
                    <div className="form-group">
                        <label htmlFor="exampleInputEmail1">Email address</label>
                        <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email"
                            value={this.state.username} onChange={(e) => this.setUsername(e.target.value)} />
                        <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
                    <div className="form-group">
                        <label htmlFor="exampleInputPassword1">Password</label>
                        <input type="password" className="form-control" id="exampleInputPassword1" placeholder="Password"
                            value={this.state.password} onChange={(e) => this.setPassword(e.target.value)} />
                    </div>
                    <div className="form-group">
                        <textarea value={this.state.comments} onChange={(e) => this.setComments(e.target.value)} className="form-control"></textarea>
                    </div>
                    <div className="form-group">
                        <select value={this.state.tipoId} onChange={(e) => this.setTipoId(e.target.value)} className="form-control">
                            <option value="">Seleccione un valor</option>
                            <option value="CC">Cédula</option>
                            <option value="TI">Tarjeta de Identidad</option>
                            <option value="RC">Registro Civil</option>
                        </select>
                    </div>
                    <div className="form-group">
                        <label className="col-lg-6">Genero</label>
                        <div className="form-check form-check-inline">
                            <input className="form-check-input" type="radio" name="inlineRadioGender" id="inlineRadioMujer"
                                value="M" onChange={(e) => this.setGender(e.target.value)} required checked={this.state.gender === 'M'} />
                            <label className="form-check-label" htmlFor="inlineRadioMujer">Mujer</label>
                        </div>
                        <div className="form-check form-check-inline">
                            <input className="form-check-input" type="radio" name="inlineRadioGender" id="inlineRadioHombre" value="H"
                                onChange={(e) => this.setGender(e.target.value)} checked={this.state.gender === 'H'} />
                            <label className="form-check-label" htmlFor="inlineRadioHombre">Hombre</label>
                        </div>
                        <div className="form-check form-check-inline">
                            <input className="form-check-input" type="radio" name="inlineRadioGender" id="inlineRadioOtro" value="O"
                                onChange={(e) => this.setGender(e.target.value)} checked={this.state.gender === 'O'} />
                            <label className="form-check-label" htmlFor="inlineRadioOtro">Otro</label>
                        </div>
                    </div>
                    <div className="formulario__grupo" id="grupo__estudios">
                        <label className="formulario__label">
                            <input className="formulario__checkbox" type="checkbox" name="estudios" id="estudios"
                                value="No" onChange={(e) => this.setEstudiante(e.target.value)}
                                checked={this.state.estudiante} />
                            Estudiando actualmente
                        </label>
                    </div>
                    <button type="submit" className="btn btn-primary">Submit</button>
                </form>
                <CommonValidateForm />
            </>
        );
    }
};

export default Form1;