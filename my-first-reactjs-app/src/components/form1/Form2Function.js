import {useState} from "react";
import Message from '../message/Message.js';
import CommonValidateForm from '../CommonValidateForm.js'
import UserRestClient from '../../integration/UserRestClient.js';
import ShowMessage from "../ShowMessage.js";

function Form2Function(props){
    let myRestClient = new UserRestClient();
    
    const [state, setState] = useState(
        {
            username: "",
            password: "",
            comments: "",
            tipoId: "",
            gender: "",
            estudiante: true,
            fullName: "",
            showError: false,
            showWarn: false,
            showSuccess: false,
            msgError: "",
            msgWarn: "",
            msgSuccess: ""
        }
    );

    const limpiarMensajes = () => {
        setState({
            ...state,
            showError: false,
            showWarn: false,
            showSuccess: false,
            msgError: "",
            msgWarn: "",
            msgSuccess: ""
        });
    };
    
    const executeSubmit = (event) => {
        event.preventDefault();
        console.log("Ambiente de ejecución: " + process.env.NODE_ENV);
        let mySecreto = process.env.REACT_APP_MY_SECRET;
        limpiarMensajes();
        console.log("Username: " + state.username + " - Password: " + state.password);
        console.log("Comments: " + state.comments + " - tipoId: " + state.tipoId);
        console.log("Gender: " + state.gender + " - estudiante: " + state.estudiante + " - fullName: " + state.fullName);

        let genderHomologado = (state.gender === "M" ? "female" : "male");
        
        const request = {
            name: state.fullName,
            email: state.username,
            gender: genderHomologado,
            status: "active"
        };
        
        myRestClient.invokeCreate(request)
            .then(userID => {
                setState({
                    ...state,
                    username: "",
                    password: "",
                    comments: "",
                    tipoId: "",
                    gender: "",
                    estudiante: true,
                    fullName: "",
                    showSuccess: true,
                    msgSuccess: "Se creo el usuario con el ID " + userID
                });
            }).catch(error => {
                setState({
                    ...state,
                    showError: true,
                    msgError: error.message
                });
            });

        //event.target.reset();
    };

    const handleOnChange = (event) => {
        let nameField = event.target.name;
        let valueField = event.target.value;

        setState({
            ...state,
            [nameField]: valueField,
        });
    };

    const setEstudiante = (estudiante) => {
        setState({
            ...state,
            estudiante: !state.estudiante
        });
    }

    return (
        <>
        <ShowMessage pintarError={state.showError} mensajeError={state.msgError} pintarWarn={state.showWarn} mensajeWarn={state.msgWarn}
            pintarSuccess={state.showSuccess} mensajeSuccess={state.msgSuccess}/>
        <Message fullName="Pedro Perez" />
        <p>El ambiente de ejecución es {process.env.NODE_ENV}</p>
        <h1>El ambiente de ejecución es %NODE_ENV%</h1>
        <h2>El valor de REACT_APP_ENV_VALUE es  {process.env.REACT_APP_ENV_VALUE}</h2>
        <p>El valor de la variable MY_APP_HOME es:  {process.env.REACT_APP_MY_HOME}</p>
        <form onSubmit={executeSubmit}>
            <div className="form-group">
                <label htmlFor="fullName">Full Name</label>
                <input type="text" className="form-control" id="fullName" aria-describedby="fullName" placeholder="Enter full name"
                    value={state.fullName} onChange={handleOnChange} name="fullName"/>
            </div>
            <div className="form-group">
                <label htmlFor="exampleInputEmail1">Email address</label>
                <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email"
                    value={state.username} onChange={handleOnChange} name="username"/>
                <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
            </div>
            <div className="form-group">
                <label htmlFor="exampleInputPassword1">Password</label>
                <input type="password" className="form-control" id="exampleInputPassword1" placeholder="Password"
                    value={state.password} onChange={handleOnChange} name="password"/>
            </div>
            <div className="form-group">
                <textarea value={state.comments} onChange={handleOnChange} className="form-control"
                    name="comments"></textarea>
            </div>
            <div className="form-group">
                <select value={state.tipoId} onChange={handleOnChange} className="form-control"
                    name="tipoId">
                    <option value="">Seleccione un valor</option>
                    <option value="CC">Cédula</option>
                    <option value="TI">Tarjeta de Identidad</option>
                    <option value="RC">Registro Civil</option>
                </select>
            </div>
            <div className="form-group">
                <label className="col-lg-6">Genero</label>
                <div className="form-check form-check-inline">
                    <input className="form-check-input" type="radio" name="gender" id="inlineRadioMujer"
                        value="M" onChange={handleOnChange} required checked={state.gender === 'M'} />
                    <label className="form-check-label" htmlFor="inlineRadioMujer">Mujer</label>
                </div>
                <div className="form-check form-check-inline">
                    <input className="form-check-input" type="radio" name="gender" id="inlineRadioHombre" value="H"
                        onChange={handleOnChange} checked={state.gender === 'H'} />
                    <label className="form-check-label" htmlFor="inlineRadioHombre">Hombre</label>
                </div>
                <div className="form-check form-check-inline">
                    <input className="form-check-input" type="radio" name="gender" id="inlineRadioOtro" value="O"
                        onChange={handleOnChange} checked={state.gender === 'O'} />
                    <label className="form-check-label" htmlFor="inlineRadioOtro">Otro</label>
                </div>
                <div className="formulario__grupo" id="grupo__estudios">
                <label className="formulario__label">
                    <input className="formulario__checkbox" type="checkbox" name="estudiante" id="estudios"
                        value="No" onChange={(e) => setEstudiante(e.target.value)}
                        checked={state.estudiante} />
                    Estudiando actualmente
                </label>
            </div>
            </div>
            <button type="submit" className="btn btn-primary">Submit</button>
        </form>
        <CommonValidateForm />
        </>
    );
};

export default Form2Function;