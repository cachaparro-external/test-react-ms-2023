import MessageFunction from "../message/MessageFunction.js";

function Form1Function(){
    return (
        <>
        <MessageFunction firstName="Pedro" lastName="Perez" />
        <p>Lista de cosas por traer</p>
        <p>{this.state.item1}</p>
        <p>{this.state.item2}</p>
        </>
    );
};

export default Form1Function;