import axios from "axios";

let noPag = 0;
let final = false;

do {
    axios.get("https://gorest.co.in/public/v2/users", {
        params: {

            page: noPag,
            per_page: 100
        },
    })
        .then(response => {

            if (response.status === 200) {
                let users = response.data;

                if (users.length >= 1) {

                    console.log("\nPagina no " + noPag + "\n");
                    users.forEach(element => console.log(element.id + " " + element.name));
                    noPag++;

                } else {
                    final = true;
                }
            }
        })
        .catch(error => {
            console.log("Se presento un error" + error);
        });

} while (final === false)

if(final=== true){
    console.log("Fin de la consulta")
}