import axios from "axios";

/*axios.get("https://gorest.co.in/public/v2/users")
    .then()//Exitoso
    .catch()//Fallido
    .finally();//Siempre se ejecuta*/

/*axios.get("https://gorest.co.in/public/v2/users")
    .then(response => {
        //Voy a colocar lo que tengo que hacer si la petición respondio entre 200 y 299
        console.log(response);
    })
    .catch(error => {
        //Voy a colocar lo que tengo que hacer si la petición respondio diferente 200 a 299 o si ocurrio un error
        console.log(error);
    })//Fallido
    .finally(() => {
        //Siempre se ejecuta
        console.log("Ejecutando finally");
    });*/

const MAIN_URL = "https://gorest.co.in/public/v2"

/*axios.get(MAIN_URL + "/users")
    .then(response => {
        let statusCode = response.status;

        if(statusCode === 200){
            let respuestaJson = response.data;

            console.log("La respuesta fue: " + JSON.stringify(respuestaJson));
            console.log("El primer nombre es: " + respuestaJson[0].name);

        }else{
            console.log("Se esperaba un 200 y se obtuvo un " + statusCode);
        }
    })
    .catch(error => {
        console.log("Se presento un error y el código retornado es " + error.response.status);
    });*/

/*axios.get(MAIN_URL + "/users" + "/3961")
    .then(response => {
        let statusCode = response.status;

        if(statusCode === 200){
            let respuestaJson = response.data;

            console.log("La respuesta fue: " + JSON.stringify(respuestaJson));
            console.log("El nombre es: " + respuestaJson.name);
        }else{
            console.log("Se esperaba un 200 y se obtuvo un " + statusCode);
        }
    })
    .catch(error => {
        if(error.response.status === 404){
            console.log("El usuario no existe");
        }else{
            console.log("Se presento un error y el código retornado es " + error.status);
        }
    });*/

/*axios.get(MAIN_URL + "/users", {
    params: {
        page: 1, 
        per_page: 5 
    },
})
    .then(response => {
        let statusCode = response.status;

        if(statusCode === 200){
            let respuestaJson = response.data;

            console.log("La respuesta fue: " + JSON.stringify(respuestaJson));
            console.log("El primer nombre es: " + respuestaJson[0].name);

        }else{
            console.log("Se esperaba un 200 y se obtuvo un " + statusCode);
        }
    })
    .catch(error => {
        console.log("Se presento un error y el código retornado es " + error.response.status);
    });*/

const TOKEN_BEARER = "9c669fe8841c74561f419e950d659c3dba47f2597c477e674272faa5132e630d";

/*const requestJson = {
    name: "Tenali Ramakrishna", 
    gender:"male", 
    email:"ing.carloschaparro@pepito2.com", 
    status:"active"
};

const config ={
    headers: {
        Authorization: "Bearer " + TOKEN_BEARER
    }
};

axios.post(MAIN_URL + "/users", requestJson, config)
    .then(response => {
        let statusCode = response.status;

        if(statusCode === 201){
            let respuestaJson = response.data;

            console.log("La respuesta fue: " + JSON.stringify(respuestaJson));
        }else{
            console.log("Se esperaba un 201 y se obtuvo un " + statusCode);
        }
    })
    .catch(error => {
        if(error.response){
            if(error.response.status === 401){
                console.log("No envio el token de autenticación")
            }else if(error.response.status === 422){
                console.log("El correo " + requestJson.email + " ya existe");
            }else{
                console.log("HTTP Code " + error.response.status);
                console.log("Se presento un error " + error);
            }
        }else{
            console.log("Se presento un error " + error);
        }
    });*/

//POST
//PUT
//DELETE

/*const config = {
    url: MAIN_URL + "/users",
    method: "GET",
    params: {
        page: 1, 
        per_page: 5 
    },
    timeout: 10000
};

axios(config)
    .then(response => { //200-299
        let statusCode = response.status;

        if(statusCode === 200){
            let respuestaJson = response.data;

            console.log("La respuesta fue: " + JSON.stringify(respuestaJson));
            console.log("El primer nombre es: " + respuestaJson[0].name);

        }else{
            console.log("Se esperaba un 200 y se obtuvo un " + statusCode);
        }
    })
    .catch(error => {
        if(error.response){//400 - 499 o 500 - 599
            console.log("Se presento un error y el código retornado es " + error.response.status);
        }else{
            console.log("Se presento un error " + error);
        }
    });*/

/*const requestJson = {
    name: "Tenali Ramakrishna", 
    gender:"male", 
    email:"ing.carloschaparro@pepito3.com", 
    status:"active"
};

const config = {
    url: MAIN_URL + "/users",
    method: "POST",
    timeout: 10000,
    data: requestJson,
    headers: {
        Authorization: "Bearer " + TOKEN_BEARER
    }
};

axios(config)
    .then(response => {
        let statusCode = response.status;

        if(statusCode === 201){
            let respuestaJson = response.data;

            console.log("La respuesta fue: " + JSON.stringify(respuestaJson));
        }else{
            console.log("Se esperaba un 201 y se obtuvo un " + statusCode);
        }
    })
    .catch(error => {
        if(error.response){
            if(error.response.status === 401){
                console.log("No envio el token de autenticación")
            }else if(error.response.status === 422){
                console.log("El correo " + requestJson.email + " ya existe");
            }else{
                console.log("HTTP Code " + error.response.status);
                console.log("Se presento un error " + error);
            }
        }else{
            console.log("Se presento un error " + error);
        }
    });*/
    


    ///////////////////////

axios.defaults.baseURL = MAIN_URL;
axios.defaults.headers.common['Authorization'] = "Bearer " + TOKEN_BEARER;
axios.defaults.timeout = 10000;

axios.get("/users" + "/3961")
    .then(response => {
        let statusCode = response.status;

        if(statusCode === 200){
            let respuestaJson = response.data;

            console.log("La respuesta fue: " + JSON.stringify(respuestaJson));
            console.log("El nombre es: " + respuestaJson.name);
        }else{
            console.log("Se esperaba un 200 y se obtuvo un " + statusCode);
        }
    })
    .catch(error => {
        if(error.response){
            if(error.response.status === 404){
                console.log("El usuario no existe");
            }else{
                console.log("Se presento un error y el código retornado es " + error);
            }
        }else{
            console.log("Se presento un error y el código retornado es " + error);
        }
    });

const requestJson = {
    name: "Tenali Ramakrishna", 
    gender:"male", 
    email:"ing.carloschaparro@pepito2.com", 
    status:"active"
};

const config ={
    headers: {
        Accept: "application/json"
    }
};

axios.post("/users", requestJson, config)
    .then(response => {
        let statusCode = response.status;

        if(statusCode === 201){
            let respuestaJson = response.data;

            console.log("La respuesta fue: " + JSON.stringify(respuestaJson));
        }else{
            console.log("Se esperaba un 201 y se obtuvo un " + statusCode);
        }
    })
    .catch(error => {
        if(error.response){
            if(error.response.status === 401){
                console.log("No envio el token de autenticación")
            }else if(error.response.status === 422){
                console.log("El correo " + requestJson.email + " ya existe");
            }else{
                console.log("HTTP Code " + error.response.status);
                console.log("Se presento un error " + error);
            }
        }else{
            console.log("Se presento un error " + error);
        }
    });

//AXIOS
//GET de un usuario
//post
//PUT
//DELETE
