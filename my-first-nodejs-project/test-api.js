import axios from "axios";

const MAIN_URL = "https://gorest.co.in/public/v2"

let page = 1;

function ejecutarConsultaPaginada(){
    console.log("Ejecutando consulta para pagina: " + page);
    
    axios.get(MAIN_URL + "/users", {
        params: {
            page: page, 
            per_page: 100 
        },
    })
        .then(response => {
            let statusCode = response.status;
    
            if(statusCode === 200){
                let respuestaJson = response.data;

                //Valido el caso Base
                if(respuestaJson.length !== 0){
                    respuestaJson.forEach(element => {
                        console.log("User: " + element.id + " - " + element.name);
                    });

                    page = page + 1;
                    console.log("Se movio a pagina: " + page);

                    ejecutarConsultaPaginada();
                }else{
                    console.log("No hay mas registros, quedo en la pagina " + page);
                    return;
                }            
            }else{
                console.log("Se esperaba un 200 y se obtuvo un " + statusCode);
            }
        })
        .catch(error => {
            console.log("Se presento un error y el código retornado es " + error.response.status);
        });
}

ejecutarConsultaPaginada();

console.log("TERMINO");
