import axios from 'axios';
import express from 'express';
import bodyParser from 'body-parser';

//Obteniendo una instancia de Express
const app = express();
app.use(bodyParser.json());

//Puerto de escucha
const port = 3000;

/*app.get("/", function(request, response){
    console.log("Recibiendo petición GET por / con request: " + request);

    response.send("Hola Mundo");    
});*/

/*const content = "<html><body><p>Hola Mundo desde GET</p></body></html>"; //JSX Javascript XML

app.get("/", (request, response) => {
    console.log("Recibiendo petición GET por / con request: " + request);

    response.send(content);    
});*/

/*let myFunction = function(request, response){
    console.log("Recibiendo petición GET por / con request: " + request);

    response.send("Hola Mundo");    
}

app.get("/", myFunction);*/

/*app.post("/", (request, response) => {
    console.log("Recibiendo petición POST por / con request: " + request);

    response.send("Hola Mundo desde POST");    
});

app.put("/", (request, response) => {
    console.log("Recibiendo petición PUT por / con request: " + request);

    response.send("Hola Mundo desde PUT");    
});

app.delete("/", (request, response) => {
    console.log("Recibiendo petición DELETE por / con request: " + request);

    response.send("Hola Mundo desde DELETE");    
});*/

app.get("/functionWithPathParam/id/:id/name/:name", (request, response) => {
    let id = request.params.id;
    let name = request.params.name;

    console.log("Recibiendo GET con PathParam id " + id + " y name: " + name);
    
    response.status(201);
    response.send("GET exitoso");
});

app.get("/functionWithQueryParam", (request, response) => {
    let id = request.query.id;
    let name = request.query.name;

    console.log("Recibiendo GET con Query Param id " + id + " y name: " + name);
    
    response.status(200);
    response.setHeader("MyHeader", "MyValue");
    response.json({
        name: "Carlos",
        lastName: "Chaparro",
        age: 20
    });

    /*response.status(200).json({
        name: "Carlos",
        lastName: "Chaparro",
        age: 20
    });*/
});


/////////////////

//Procesa cualquier petición
//Util para agentes
/*app.all("/agent", (request, response, next) => {
    console.log("Applying agent...");
    next();
});

app.get("/agent", (request, response) => {
    console.log("Executing GET");

    response.send("Response GET");
});

app.route("/")
    .get((req, res) => {
        res.send("GET");
    })
    .post((req, res) => {
        res.send("POST");
    })
    .put((req, res) => {
        res.send("PUT");
    })
    .delete((req, res) => {
        res.send("DELETE");
    });


const logFunction = (req, res, next) => {
    console.log("Request: " + req.body);
    next();
};

const dateFunction = (req, res, next) => {
    let now = Date.now();
    console.log("Time request: " + now);
    //Set date in request
    req.requestDate = now;
    next();
};

app.use(logFunction);
app.use(dateFunction);

app.post("/create", (req, res) => {
    //Body
    let user = req.body;
    //Headers
    let headers = req.headers;

    console.log("Request: " + JSON.stringify(user));
    console.log("Name: " + user.firstName + " " + user.lastName);
    console.log("Header: " + JSON.stringify(headers));
    console.log("Header content-type: " + headers['content-type']);
    console.log("Request Date: " + req.requestDate);

    res.status(201).json({
        id: 2542
    });
});*/

const MAIN_URL = "https://gorest.co.in/public/v2/users";
const TOKEN_BEARER = "9c669fe8841c74561f419e950d659c3dba47f2597c477e674272faa5132e630d";

//Converting like Array
/*const setHeaders = (headers, response) => {
    if(headers){
        let headerArray = Object.entries(headers);

        if(headerArray.length > 0){
            headerArray.forEach(header => {
                const [headerName, headerValue] = header;
                
                console.log("Header Name: " + headerName, "Header Value: " + headerValue);

                if(headerName !== "transfer-encoding"){
                    response.setHeader(headerName, headerValue);
                }else{
                    console.log("Omiting " + headerName + " header");
                }  
            });
        }
    } 
};*/

//Converting like Map
/*const setHeaders = (headers, response) => {
    if(headers){
        let headerMap = new Map(Object.entries(headers));

        if(headerMap.size > 0){
            headerMap.forEach((value, key) => {
                const [headerName, headerValue] = [key, value];
                
                console.log("Header Name: " + headerName, "Header Value: " + headerValue);

                if(headerName !== "transfer-encoding"){
                    response.setHeader(headerName, headerValue);
                }else{
                    console.log("Omiting " + headerName + " header");
                }  
            });
        }
    } 
};

app.get("/users", (req, res) => {
    axios.get(MAIN_URL)
        .then(response => {
            res.status(response.status);
            setHeaders(response.headers, res);

            if(response.status === 200){
                res.json(response.data);
            }else{
                res.send();
            }
        })
        .catch(error => {
            if(error.response){
                res.status(response.status);
                setHeaders(error.response.headers, res);
                res.send();
            }else{
                res.status(500).send(error.message);
            }
        });
});

app.get("/users/:userId", (req, res) => {
    axios.get(MAIN_URL + "/" + req.params.userId)
        .then(response => {
            res.status(response.status);
            setHeaders(response.headers, res);

            if(response.status === 200){
                res.json(response.data);
            }else{
                res.send();
            }
        })
        .catch(error => {
            if(error.response){
                res.status(error.response.status);
                setHeaders(error.response.headers, res);
                res.send();
            }else{
                res.status(500).send(error.message);
            }
        });
});*/

/**
app.post("/users", (req, res) => {
    let user = req.body;

    const config = {
        headers: {
            Authorization: "Bearer " + TOKEN_BEARER
        }
    };
    
    axios.post(MAIN_URL, user, config)
        .then(response => {
            res.status(response.status);
            setHeaders(response.headers, res);

            if(response.status === 201){
                res.json(response.data);
            }else{
                res.send();
            }
        })
        .catch(error => {
            if(error.response){
                res.status(error.response.status);
                setHeaders(error.response.headers, res);
                
                /*if(error.response.status === 422){
                    res.send(error.response.data[0].field + " " + error.response.data[0].message);
                }else{
                    res.send();
                }*//*
                if(error.response.data){
                    res.json(error.response.data);
                }else{
                    res.send();
                }
            }else{
                res.status(500).send(error.message);
            }
        });
});*/


//////////////

/*app.get("/users/id/:id", (req, res) => {
    //let id = req.params.id;
    
    axios.get(MAIN_URL + "/" + req.params.id)
        .then(response => {
            res.status(response.status);

            if(response.data){
                res.json(response.data);
            }else{
                res.send();
            }
        }).catch(error => {
            if(error.response){
                res.status(error.response.status);

                if(error.response.data){
                    res.json(error.response.data);
                }else{
                    res.send();
                }
            }else{
                res.status(500).send(error.message);
            }
        });
});

app.post("/users", (req, res) => {
    //let id = req.params.id;
    //let myBody = req.body;

    const config = {
        headers: {
            Authorization: "Bearer " + TOKEN_BEARER
        }
    };
    
    axios.post(MAIN_URL, req.body, config)
        .then(response => {
            res.status(response.status);

            if(response.data){
                res.json(response.data);
            }else{
                res.send();
            }
        }).catch(error => {
            if(error.response){
                res.status(error.response.status);

                if(error.response.data){
                    res.json(error.response.data);
                }else{
                    res.send();
                }
            }else{
                res.status(500).send(error.message);
            }
        });
});*/

app.get("/pepito", (req, res) => {
    console.log("GET pepito");
    res.send("Responde GET pepito");
});

app.get("/pepito", (req, res) => {
    console.log("POST pepito");
    res.send("Responde POST pepito");
});

app.route("/pepito")
    .get((req, res) => {
        console.log("GET pepito");
        res.send("Responde GET pepito");
    })
    .post((req, res) => {
        console.log("POST pepito");
        res.send("Responde POST pepito");
    });

//Subir el servidor
app.listen(port, () => {
    console.log("Server running on port " + port);
});