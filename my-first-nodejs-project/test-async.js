import { default as axios } from "axios";

console.log("Ejecutando mi programa");

/*function doSomething(){
    console.log("Entrando a la función")

    axios.get("https://gorest.co.in/public/v2/users")
        .then(response => {
            console.log("Haciendo la tarea");
            console.log(response.data);
        })
        .catch(error => {
            console.error(error);
        });
    /*setTimeout(() => {
        console.log("Haciendo la tarea");
    }, 10000);*/

    /*console.log("Saliendo de la función")
};*/

/*async function doSomething(){
    console.log("Entrando a la función")

    await axios.get("https://gorest.co.in/public/v2/users")
        .then(response => {
            console.log("Haciendo la tarea");
            console.log(response.data);
        })
        .catch(error => {
            console.error(error);
        });

    console.log("Saliendo de la función")
};*/

async function doSomething(){
    console.log("Entrando a la función");

    return axios.get("https://gorest.co.in/public/v2/users")
        .then(response => {
            console.log("Saliendo de la función");
            return response.data;
        })
        .catch(error => {
            console.log("Saliendo de la función");
            throw new Error("Ocurrio un error");
        });
};

console.log("Despues de definir la función");

doSomething()
    .then(users => {
        console.log("Los usuario son " + users);
    })
    .catch(error => {
        console.error(error.message);
    });

console.log("Finalizando mi programa");